# HijriCalendar

You can have the hijri date and prayer time from this application. This application uses [Aladhan API](https://aladhan.com/prayer-times-api) for getting those information.

<img src="preview.png">

**N.B. This app will only show the date of Saudi Arabia and your location represents your timezone. Some countries may need one or two days adjustment with the date of Saudi Arabia.**

# Dependency
 
* [Qt Framework](https://www.qt.io/)

# Build 

Build Hijri Calendar from source.

## For Arch user

```
# Install git and dependency
sudo pacman -S git qt5-base

# Clone the repository
git clone https://gitlab.com/s96Abrar/hijricalendar
cd hijricalendar

# Build the source
qmake -qt5 && make

# Run hijricalendar
./hijricalendar

# To install hijricalendar
sudo make install
```

## For debian user

```
# Install git
sudo apt-get install git

# Install dependency
sudo apt-get install qtbase5-dev

# Clone the repository
git clone https://gitlab.com/s96Abrar/hijricalendar
cd hijricalendar

# Build the source
qmake -qt5 && make

# Run hijricalendar
./hijricalendar

# To install hijricalendar
sudo make install
```
