/*
    HijriCalendar
    Copyright (C) 2020 Abrar

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
    USA
*/

#include <QScreen>
#include <QDebug>
#include <QDateTime>
#include <QApplication>
#include <QMenu>
#include <QCloseEvent>

#include "location.h"
#include "addlocation.h"

#include "calendar.h"
#include "ui_calendar.h"

/*
11. Timings - http://api.aladhan.com/v1/timings/:date_or_timestamp

    Description:

    Returns all prayer times for a specific date.
    Method: GET
    Parameters:
        "date_or_timestamp" (string) - A date in the DD-MM-YYYY format or UNIX timestamp. Default's to the current date.
        "latitude" (decimal)         - The decimal value for the latitude co-ordinate of the location you want the time computed for. Example: 51.75865125
        "longitude" (decimal)        - The decimal value for the longitude co-ordinate of the location you want the time computed for. Example: -1.25387785
        "method" (number)            - A prayer times calculation method. Methods identify various schools of thought about how to compute the timings.
                                       This parameter accepts values from 0-12 and 99, as specified below:
            0 - Shia Ithna-Ansari
            1 - University of Islamic Sciences, Karachi
            2 - Islamic Society of North America
            3 - Muslim World League
            4 - Umm Al-Qura University, Makkah
            5 - Egyptian General Authority of Survey
            7 - Institute of Geophysics, University of Tehran
            8 - Gulf Region
            9 - Kuwait
            10 - Qatar
            11 - Majlis Ugama Islam Singapura, Singapore
            12 - Union Organization islamic de France
            13 - Diyanet İşleri Başkanlığı, Turkey
            14 - Spiritual Administration of Muslims of Russia
            99 - Custom. See https://aladhan.com/calculation-methods

        "tune" (string) - Comma Separated String of integers to offset timings returned by the API in minutes. Example: 5,3,5,7,9,7. See https://aladhan.com/calculation-methods
        "school" (number) - 0 for Shafi (or the standard way), 1 for Hanafi. If you leave this empty, it defaults to Shafii.
        "midnightMode" (number) - 0 for Standard (Mid Sunset to Sunrise), 1 for Jafari (Mid Sunset to Fajr). If you leave this empty, it defaults to Standard.
        "timezonestring" (string) - A valid timezone name as specified on http://php.net/manual/en/timezones.php . Example: Europe/London. If you do not specify this, we'll calcuate it using the co-ordinates you provide.
        "latitudeAdjustmentMethod" (number) - Method for adjusting times higher latitudes - for instance, if you are checking timings in the UK or Sweden.
            1 - Middle of the Night
            2 - One Seventh
            3 - Angle Based

        "adjustment" (number) - Number of days to adjust hijri date(s). Example: 1 or 2 or -1 or -2

    Endpoint URL: http://api.aladhan.com/v1/timings/:date_or_timestamp
    Example Request: http://api.aladhan.com/v1/timings/1398332113?latitude=51.508515&longitude=-0.1254872&method=2
*/

calendar::calendar(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::calendar)
{
    ui->setupUi(this);


#if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
	qRegisterMetaType<CalendarData>("CalendarData");
    qRegisterMetaTypeStreamOperators<CalendarData>("CalendarData");
#endif


#if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
	qRegisterMetaType<LocationData>("LocationData");
    qRegisterMetaTypeStreamOperators<LocationData>("LocationData");
#endif

    ui->stackedWidget->setCurrentIndex(0);
    qDebug() << "Version " << VERSION_TXT;

    startSetup();
    loadSettings();
}

calendar::~calendar()
{
	m_locations.clear();	
	delete trayicon;
    delete ui;
}

void calendar::closeEvent(QCloseEvent *event)
{
	if (QSystemTrayIcon::isSystemTrayAvailable() && trayicon->isVisible()) {
        hide();
        event->ignore();
		return;
	} else {
		qApp->quit();
	}

	QWidget::closeEvent(event);
}

void calendar::timerEvent(QTimerEvent *event)
{
	if (event->timerId() == timer.timerId()) {
		on_reload_clicked();
	}

	event->accept();
}

void calendar::startSetup()
{
	// 1 Hour update
	timer.start(1000 * 60 * 60, this);

	if (QSystemTrayIcon::isSystemTrayAvailable()) {
		trayicon = new QSystemTrayIcon(QIcon::fromTheme("hijricalendar"), this);

		QMenu *traymenu = new QMenu(this);
		traymenu->addAction(QIcon(), "&Toggle Visible", this, SLOT(toggleVisible()));
		traymenu->addSeparator();
		traymenu->addAction(QIcon::fromTheme("application-exit"), "&Quit", QCoreApplication::instance(), SLOT(quit()));

		trayicon->setContextMenu(traymenu);
		trayicon->setToolTip("");
		trayicon->show();
		connect(trayicon, &QSystemTrayIcon::activated, this, &calendar::showWindow);
	} else {
		qDebug() << "No system tray found.";
		trayicon = nullptr;
		show();
	}
}

void calendar::loadSettings()
{
    QString sep = "";
    QStringList groups = settings->childGroups();

    if (!groups.count()) {
        show();
        return;
    }

	for (const QString &group : groups) {
        addLocation(sep, settings->value(group + "/locationdata").value<LocationData>(),
                    settings->value(group + "/calendardata").value<CalendarData>());
        sep = "\n";
    }
}

void calendar::addLocation(const QString &sep, LocationData ld, CalendarData cd)
{
	Location *loc = new Location(this);
    loc->setLocationData(ld);

    if (cd.lat().count() && cd.lon().count()) {
        loc->setCalendarData(cd);
    }

    loc->reloadData();

    ui->containerLayout->insertWidget(ui->containerLayout->count() - 1, loc);

    m_locations.append(loc);
    ui->stackedWidget->setCurrentIndex(1);

	if (trayicon)
		trayicon->setToolTip(trayicon->toolTip() + sep + loc->trayTooltip());

	connect(loc, &Location::updated, this, &calendar::locationUpdated);
    connect(loc, &Location::deleted, this, &calendar::deleteLocation);
}

void calendar::showWindow(QSystemTrayIcon::ActivationReason reason)
{
    if (reason == QSystemTrayIcon::DoubleClick || reason == QSystemTrayIcon::Trigger) {
        toggleVisible();
    }
}

void calendar::on_addNewLoc_clicked()
{
    AddLocation *addLoc = new AddLocation(this);
    addLoc->setLabel("Add new location");

    QString sep = "";
    if (addLoc->exec() == QDialog::Accepted) {
        addLocation(sep, addLoc->locationData(), CalendarData());
        sep = "\n";
    }
}

void calendar::on_reload_clicked()
{
	Q_FOREACH (Location *loc, m_locations) {
        loc->reloadData();
    }
}

void calendar::locationUpdated()
{
	if (trayicon) {
		QString sep = "";
		Q_FOREACH (Location *loc, m_locations) {
			trayicon->setToolTip(sep + loc->trayTooltip());
			sep = "\n";
		}
	}
}

void calendar::deleteLocation(Location *loc)
{
    m_locations.removeOne(loc);
    ui->containerLayout->removeWidget(loc);

    settings->remove(QString("%1,%2").arg(loc->locationData().lat()).arg(loc->locationData().lon()));
    loc->deleteLater();

    if (ui->containerLayout->count() == 1) {
        ui->stackedWidget->setCurrentIndex(0);
    }
}

void calendar::toggleVisible()
{
    if (isVisible()) {
        hide();
    } else {
        show();
	}
}
