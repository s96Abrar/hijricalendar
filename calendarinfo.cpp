/*
	HijriCalendar
	Copyright (C) 2020 Abrar

	This library is free software; you can redistribute it and/or
	modify it under the terms of the GNU Lesser General Public
	License as published by the Free Software Foundation; either
	version 2.1 of the License, or (at your option) any later version.

	This library is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
	Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public
	License along with this library; if not, write to the Free Software
	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
	USA
*/

#include <QFile>
#include <QDebug>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include <QJsonValue>

#include "calendarinfo.h"

CalendarInfo::CalendarInfo(const QString &filePath, QObject *parent) :
	QObject(parent),
	m_filePath(filePath)
{
}

void CalendarInfo::initializeParse()
{
	qDebug() << "Initializing parse...";

	QFile file(m_filePath);
	file.open(QIODevice::ReadOnly);
	parse(file.readAll());
	file.close();
	qDebug() << "File Deleted: " << file.remove();
}

CalendarData CalendarInfo::getCalendarData()
{
	return m_cData;
}

void CalendarInfo::parse(QByteArray data)
{
	QJsonDocument doc = QJsonDocument::fromJson(data);

	if (!doc.isObject()) {
		return;
	}

	QJsonObject obj;
	QJsonObject tObj;

	QJsonArray arr;
	QJsonValue val;

	obj = doc.object();

	if (obj.value("status").toString() == "OK") {
		obj = obj.value("data").toObject();
		tObj = obj.value("timings").toObject();

		m_cData.setFijrTime(tObj.value("Fajr").toString());
		m_cData.setSunriseTime(tObj.value("Sunrise").toString());
		m_cData.setDhuhrTime(tObj.value("Dhuhr").toString());
		m_cData.setAsrTime(tObj.value("Asr").toString());
		m_cData.setSunsetTime(tObj.value("Sunset").toString());
		m_cData.setMaghribTime(tObj.value("Maghrib").toString());
		m_cData.setIshaTime(tObj.value("Isha").toString());
		m_cData.setImsakTime(tObj.value("Imsak").toString());
		m_cData.setMidnightTime(tObj.value("Midnight").toString());

		tObj = obj.value("date").toObject();
		tObj = tObj.value("hijri").toObject();

		m_cData.setHijriDate(tObj.value("date").toString());
		m_cData.setHijriDay(tObj.value("day").toString());
		m_cData.setHijriWeekday(tObj.value("weekday").toObject().value("en").toString());
		m_cData.setHijriMonthNumber(tObj.value("month").toObject().value("number").toString());
		m_cData.setHijriMonthName(tObj.value("month").toObject().value("en").toString());
		m_cData.setHijriYear(tObj.value("year").toString());

		arr = tObj.value("holidays").toArray();
		QString temp = "";

		for (int i = 0; i < arr.count(); i++) {
			temp += arr.at(i).toString();
		}

		if (temp.count()) {
			temp.resize(temp.size() - 1);
		}

		m_cData.setHijriHolidays(temp);

		tObj = obj.value("date").toObject();
		tObj = tObj.value("gregorian").toObject();
		m_cData.setGregorianDate(tObj.value("date").toString());

		tObj = obj.value("meta").toObject();
		m_cData.setLat(tObj.value("latitude").toDouble());
		m_cData.setLon(tObj.value("longitude").toDouble());
	}
}

