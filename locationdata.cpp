/*
	HijriCalendar
	Copyright (C) 2020 Abrar

	This library is free software; you can redistribute it and/or
	modify it under the terms of the GNU Lesser General Public
	License as published by the Free Software Foundation; either
	version 2.1 of the License, or (at your option) any later version.

	This library is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
	Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public
	License along with this library; if not, write to the Free Software
	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
	USA
*/

#include "locationdata.h"

LocationData::LocationData()
	: m_city(""),
	  m_country(""),
	  m_lat(""),
	  m_lon("")
{
}

QDataStream &operator<<(QDataStream &out, const LocationData &data)
{
	out << data.m_city << data.m_country << data.m_lat << data.m_lon;
	return out;
}

QDataStream &operator>>(QDataStream &in, LocationData &data)
{
	in >> data.m_city >> data.m_country >> data.m_lat >> data.m_lon;
	return in;
}

QString LocationData::country() const
{
	return m_country;
}

void LocationData::setCountry(const QString &country)
{
	m_country = country;
}

QString LocationData::city() const
{
	return m_city;
}

void LocationData::setCity(const QString &city)
{
	m_city = city;
}

QString LocationData::lat() const
{
	return m_lat;
}

void LocationData::setLat(const QString &lat)
{
	m_lat = lat;
}

QString LocationData::lon() const
{
	return m_lon;
}

void LocationData::setLon(const QString &lon)
{
	m_lon = lon;
}
