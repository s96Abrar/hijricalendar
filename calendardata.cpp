/*
	HijriCalendar
	Copyright (C) 2020 Abrar

	This library is free software; you can redistribute it and/or
	modify it under the terms of the GNU Lesser General Public
	License as published by the Free Software Foundation; either
	version 2.1 of the License, or (at your option) any later version.

	This library is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
	Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public
	License along with this library; if not, write to the Free Software
	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
	USA
*/

#include "calendardata.h"

CalendarData::CalendarData()
	: m_lat(""),
	  m_lon("")
{
}

QDataStream &operator<<(QDataStream &out, const CalendarData &data)
{
	out << data.m_lat << data.m_lon << data.m_hijriWeekday << data.m_hijriDay << data.m_hijriMonthName
		<< data.m_hijriMonthNumber << data.m_hijriYear << data.m_hijriDate << data.m_gregorianDate
		<< data.m_hijriHolidays << data.m_FijrTime << data.m_DhuhrTime << data.m_AsrTime
		<< data.m_MaghribTime << data.m_IshaTime << data.m_sunriseTime << data.m_sunsetTime
		<< data.m_ImsakTime << data.m_midnightTime;
	return out;
}

QDataStream &operator>>(QDataStream &in, CalendarData &data)
{
	in >> data.m_lat >> data.m_lon >> data.m_hijriWeekday >> data.m_hijriDay >> data.m_hijriMonthName
	   >> data.m_hijriMonthNumber >> data.m_hijriYear >> data.m_hijriDate >> data.m_gregorianDate
	   >> data.m_hijriHolidays >> data.m_FijrTime >> data.m_DhuhrTime >> data.m_AsrTime
	   >> data.m_MaghribTime >> data.m_IshaTime >> data.m_sunriseTime >> data.m_sunsetTime
	   >> data.m_ImsakTime >> data.m_midnightTime;
	return in;
}

QString CalendarData::lat() const
{
	return m_lat;
}

void CalendarData::setLat(const float lat)
{
	m_lat = QString::number(lat);
}

QString CalendarData::lon() const
{
	return m_lon;
}

void CalendarData::setLon(const float lon)
{
	m_lon = QString::number(lon);
}

QString CalendarData::hijriWeekday() const
{
	return m_hijriWeekday;
}

void CalendarData::setHijriWeekday(const QString &hijriWeekday)
{
	m_hijriWeekday = hijriWeekday;
}

QString CalendarData::hijriDay() const
{
	return m_hijriDay;
}

void CalendarData::setHijriDay(const QString &hijriDay)
{
	m_hijriDay = hijriDay;
}

QString CalendarData::hijriMonthName() const
{
	return m_hijriMonthName;
}

void CalendarData::setHijriMonthName(const QString &hijriMonthName)
{
	m_hijriMonthName = hijriMonthName;
}

QString CalendarData::hijriMonthNumber() const
{
	return m_hijriMonthNumber;
}

void CalendarData::setHijriMonthNumber(const QString &hijriMonthNumber)
{
	m_hijriMonthNumber = hijriMonthNumber;
}

QString CalendarData::hijriYear() const
{
	return m_hijriYear;
}

void CalendarData::setHijriYear(const QString &hijriYear)
{
	m_hijriYear = hijriYear;
}

QString CalendarData::hijriDate() const
{
	return m_hijriDate;
}

void CalendarData::setHijriDate(const QString &hijriDate)
{
	m_hijriDate = hijriDate;
}

QString CalendarData::hijriHolidays() const
{
	return m_hijriHolidays;
}

void CalendarData::setHijriHolidays(const QString &hijriHolidays)
{
	m_hijriHolidays = hijriHolidays;
}

QString CalendarData::FijrTime() const
{
	return m_FijrTime;
}

void CalendarData::setFijrTime(const QString &FijrTime)
{
	m_FijrTime = FijrTime;
}

QString CalendarData::DhuhrTime() const
{
	return m_DhuhrTime;
}

void CalendarData::setDhuhrTime(const QString &DhuhrTime)
{
	m_DhuhrTime = DhuhrTime;
}

QString CalendarData::AsrTime() const
{
	return m_AsrTime;
}

void CalendarData::setAsrTime(const QString &AsrTime)
{
	m_AsrTime = AsrTime;
}

QString CalendarData::MaghribTime() const
{
	return m_MaghribTime;
}

void CalendarData::setMaghribTime(const QString &MaghribTime)
{
	m_MaghribTime = MaghribTime;
}

QString CalendarData::IshaTime() const
{
	return m_IshaTime;
}

void CalendarData::setIshaTime(const QString &IshaTime)
{
	m_IshaTime = IshaTime;
}

QString CalendarData::sunriseTime() const
{
	return m_sunriseTime;
}

void CalendarData::setSunriseTime(const QString &sunriseTime)
{
	m_sunriseTime = sunriseTime;
}

QString CalendarData::sunsetTime() const
{
	return m_sunsetTime;
}

void CalendarData::setSunsetTime(const QString &sunsetTime)
{
	m_sunsetTime = sunsetTime;
}

QString CalendarData::ImsakTime() const
{
	return m_ImsakTime;
}

void CalendarData::setImsakTime(const QString &ImsakTime)
{
	m_ImsakTime = ImsakTime;
}

QString CalendarData::midnightTime() const
{
	return m_midnightTime;
}

void CalendarData::setMidnightTime(const QString &midnightTime)
{
	m_midnightTime = midnightTime;
}

QString CalendarData::gregorianDate() const
{
	return m_gregorianDate;
}

void CalendarData::setGregorianDate(const QString &gregorianDate)
{
	m_gregorianDate = gregorianDate;
}
