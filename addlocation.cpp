/*
    HijriCalendar
    Copyright (C) 2020 Abrar

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
    USA
*/

#include "addlocation.h"
#include "ui_addlocation.h"

AddLocation::AddLocation(QWidget *parent) :
	QDialog(parent),
	ui(new Ui::AddLocation)
{
	ui->setupUi(this);
}

AddLocation::~AddLocation()
{
	delete ui;
}

void AddLocation::setLabel(QString header)
{
	ui->header->setText(header);
}

LocationData AddLocation::locationData() const
{
	return m_locationData;
}

void AddLocation::setLocationData(const LocationData &locationData)
{
	m_locationData = locationData;

	ui->sCountry->setText(m_locationData.country());
	ui->sCity->setText(m_locationData.city());
	ui->sLat->setText(m_locationData.lat());
	ui->sLon->setText(m_locationData.lon());
}

void AddLocation::on_sCity_textChanged(const QString &arg1)
{
	if (arg1.count()) {
		if (ui->sCountry->text().count()) {
			ui->select->setEnabled(true);
			return;
		}
	}

	ui->select->setEnabled(false);
}

void AddLocation::on_sCountry_textChanged(const QString &arg1)
{
	if (arg1.count()) {
		if (ui->sCity->text().count()) {
			ui->select->setEnabled(true);
			return;
		}
	}

	ui->select->setEnabled(false);
}

void AddLocation::on_select_clicked()
{
	if ((ui->sLat->text().count() && !ui->sLon->text().count()) ||
			(!ui->sLat->text().count() && ui->sLon->text().count())) {
		ui->status->setText("Please enter both latitude and longitude.");
		return;
	}

	m_locationData.setCountry(ui->sCountry->text());
	m_locationData.setCity(ui->sCity->text());
	m_locationData.setLat(ui->sLat->text());
	m_locationData.setLon(ui->sLon->text());

	emit newLocation(m_locationData);

	accept();
}

void AddLocation::on_cancel_clicked()
{
	reject();
}
