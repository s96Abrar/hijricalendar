/*
    HijriCalendar
    Copyright (C) 2020 Abrar

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
    USA
*/

#include "calendar.h"
#include "qtsingleapplication.h"

#include <QCommandLineParser>
#include <QDebug>

int main(int argc, char *argv[])
{
#if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
	QApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
#endif

	QtSingleApplication app("HijriCalendar", argc, argv);
	if (app.isRunning()) {
		qDebug() << "Hijri Calendar is already running.";
		return not app.sendMessage("");
	}

	app.setOrganizationName("mini");
	app.setApplicationName(APPNAME);
	app.setApplicationVersion(VERSION_TXT);
	app.setQuitOnLastWindowClosed(false);

	QCommandLineParser parser;
	parser.addHelpOption();
	parser.addVersionOption();

	parser.setApplicationDescription("A simple app for showing hijri date and prayer times using Aladhan API (https://aladhan.com/prayer-times-api)");

	parser.process(app);

	calendar w;

	return app.exec();
}
