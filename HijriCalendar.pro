QT       += widgets network

TARGET = hijricalendar
TEMPLATE = app
VERSION = 2.0.1

CONFIG += c++11 silent

DEFINES += VERSION_TXT=\"\\\"$${VERSION}\\\"\"
DEFINES += APPNAME=\"\\\"$${TARGET}\\\"\"

unix {
		isEmpty(PREFIX) {
				PREFIX = /usr
		}

		target.path = $$PREFIX/bin/

		desktop.path  = $$PREFIX/share/applications/
		desktop.files = hijricalendar.desktop

                icons.path    = $$PREFIX/share/icons/hicolor/scalable/apps/
		icons.files   = hijricalendar.svg

if (!equals(PREFIX, /usr)) {
                autostart.path  = $$PREFIX/etc/xdg/autostart
		autostart.files = hijricalendar.desktop
} else {
                autostart.path  = /etc/xdg/autostart
                autostart.files = hijricalendar.desktop
}

		INSTALLS     += target icons desktop autostart
}

SOURCES += \
        addlocation.cpp \
	calendardata.cpp \
	calendarinfo.cpp \
	focusframe.cpp \
	location.cpp \
	locationdata.cpp \
	main.cpp \
	calendar.cpp

HEADERS += \
        addlocation.h \
	calendar.h \
	calendardata.h \
	calendarinfo.h \
	focusframe.h \
	global.h \
	location.h \
	locationdata.h

FORMS += \
	addlocation.ui \
	calendar.ui \
	location.ui
