/*
	HijriCalendar
	Copyright (C) 2020 Abrar

	This library is free software; you can redistribute it and/or
	modify it under the terms of the GNU Lesser General Public
	License as published by the Free Software Foundation; either
	version 2.1 of the License, or (at your option) any later version.

	This library is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
	Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public
	License along with this library; if not, write to the Free Software
	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
	USA
*/

#pragma once

#include <QWidget>
#include <QBasicTimer>
#include <QSystemTrayIcon>

#include "calendardata.h"
#include "locationdata.h"

class Location;

typedef QList<Location*> LocationList;

QT_BEGIN_NAMESPACE
namespace Ui {
	class calendar;
}
QT_END_NAMESPACE

class calendar : public QWidget {
	Q_OBJECT

public:
	calendar(QWidget *parent = nullptr);
	~calendar();

protected:
	void closeEvent(QCloseEvent *event) override;
	void timerEvent(QTimerEvent *event) override;

private slots:
	void on_addNewLoc_clicked();
	void on_reload_clicked();
	void toggleVisible();

private:
	Ui::calendar *ui;
	QBasicTimer timer;
	LocationList m_locations;

	void locationUpdated();
	void deleteLocation(Location *loc);

	QSystemTrayIcon *trayicon;

	void startSetup();
	void loadSettings();
	void addLocation(const QString &sep, LocationData ld, CalendarData cd);
	void showWindow(QSystemTrayIcon::ActivationReason reason);
};
