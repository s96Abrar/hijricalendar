/*
	HijriCalendar
	Copyright (C) 2020 Abrar

	This library is free software; you can redistribute it and/or
	modify it under the terms of the GNU Lesser General Public
	License as published by the Free Software Foundation; either
	version 2.1 of the License, or (at your option) any later version.

	This library is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
	Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public
	License along with this library; if not, write to the Free Software
	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
	USA
*/

#include <QDebug>

#include "focusframe.h"

focusFrame::focusFrame(QWidget *parent) :
	QFrame(parent)
{
}

focusFrame::~focusFrame()
{
	m_buttons.clear();
}

void focusFrame::enterEvent(QEvent *event)
{
	updateVisibility(true);
	QFrame::enterEvent(event);
}

void focusFrame::leaveEvent(QEvent *event)
{
	updateVisibility(false);
	QFrame::leaveEvent(event);
}

void focusFrame::updateVisibility(bool visible)
{
	Q_FOREACH( QToolButton *btn, m_buttons ) {
		btn->setVisible(visible);
	}
}

void focusFrame::setButtons(const QList<QToolButton *> &buttons)
{
	m_buttons = buttons;

	updateVisibility(false);
}
