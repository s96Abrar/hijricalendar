/*
    HijriCalendar
    Copyright (C) 2020 Abrar

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
    USA
*/

#pragma once

#include <QWidget>
#include <QNetworkAccessManager>

#include "locationdata.h"
#include "calendardata.h"
#include "global.h"

namespace Ui {
	class Location;
}

class Location : public QWidget {
	Q_OBJECT

public:
	explicit Location(QWidget *parent = nullptr);
	~Location();

	QString trayTooltip();

	LocationData locationData() const;
	void setLocationData(const LocationData &locationData);

	CalendarData calendarData() const;
	void setCalendarData(const CalendarData &calendarData);

	void reloadData();
	void saveData();

signals:
	void updated();
	void deleted(Location *);

private slots:
	void on_showInfo_toggled(bool checked);
	void on_editLoc_clicked();
	void on_deleteLoc_clicked();

private:
	Ui::Location *ui;
	QNetworkAccessManager *nam;
	QString m_fp;
	LocationData m_locationData;
	CalendarData m_calendarData;
	int m_retries;

	void setData();
	void parseNetworkReply(QNetworkReply *reply);
	void parseRequest();
	QString formatTime(QString time);
	bool isOld();
	QString callURL(QString lat, QString lon);
	QString callURL2(QString city, QString country);
};
