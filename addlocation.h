/*
	HijriCalendar
	Copyright (C) 2020 Abrar

	This library is free software; you can redistribute it and/or
	modify it under the terms of the GNU Lesser General Public
	License as published by the Free Software Foundation; either
	version 2.1 of the License, or (at your option) any later version.

	This library is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
	Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public
	License along with this library; if not, write to the Free Software
	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
	USA
*/

#pragma once

#include <QDialog>

#include "locationdata.h"

namespace Ui {
	class AddLocation;
}

class AddLocation : public QDialog {
	Q_OBJECT

public:
	explicit AddLocation(QWidget *parent = nullptr);
	~AddLocation();

	void setLabel(QString header);

	LocationData locationData() const;
	void setLocationData(const LocationData &locationData);

signals:
	void newLocation(LocationData locationData);

public slots:
	void on_sCity_textChanged(const QString &arg1);
	void on_sCountry_textChanged(const QString &arg1);
	void on_select_clicked();

private slots:
	void on_cancel_clicked();

private:
	Ui::AddLocation *ui;

	LocationData m_locationData;
};
