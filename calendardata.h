/*
	HijriCalendar
	Copyright (C) 2020 Abrar

	This library is free software; you can redistribute it and/or
	modify it under the terms of the GNU Lesser General Public
	License as published by the Free Software Foundation; either
	version 2.1 of the License, or (at your option) any later version.

	This library is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
	Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public
	License along with this library; if not, write to the Free Software
	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
	USA
*/

#pragma once

#include <QDataStream>
#include <QObject>

class CalendarData {
public:
	CalendarData();

	friend QDataStream &operator<<(QDataStream &out, const CalendarData &data);
	friend QDataStream &operator>>(QDataStream &in, CalendarData &data);

	QString lat() const;
	void setLat(const float lat);

	QString lon() const;
	void setLon(const float lon);

	QString hijriWeekday() const;
	void setHijriWeekday(const QString &hijriWeekday);

	QString hijriDay() const;
	void setHijriDay(const QString &hijriDay);

	QString hijriMonthName() const;
	void setHijriMonthName(const QString &hijriMonthName);

	QString hijriMonthNumber() const;
	void setHijriMonthNumber(const QString &hijriMonthNumber);

	QString hijriYear() const;
	void setHijriYear(const QString &hijriYear);

	QString hijriDate() const;
	void setHijriDate(const QString &hijriDate);

	QString hijriHolidays() const;
	void setHijriHolidays(const QString &hijriHolidays);

	QString FijrTime() const;
	void setFijrTime(const QString &FijrTime);

	QString DhuhrTime() const;
	void setDhuhrTime(const QString &DhuhrTime);

	QString AsrTime() const;
	void setAsrTime(const QString &AsrTime);

	QString MaghribTime() const;
	void setMaghribTime(const QString &MaghribTime);

	QString IshaTime() const;
	void setIshaTime(const QString &IshaTime);

	QString sunriseTime() const;
	void setSunriseTime(const QString &sunriseTime);

	QString sunsetTime() const;
	void setSunsetTime(const QString &sunsetTime);

	QString ImsakTime() const;
	void setImsakTime(const QString &ImsakTime);

	QString midnightTime() const;
	void setMidnightTime(const QString &midnightTime);

	QString gregorianDate() const;
	void setGregorianDate(const QString &gregorianDate);

private:
	QString m_lat;
	QString m_lon;
	QString m_hijriWeekday;
	QString m_hijriDay;
	QString m_hijriMonthName;
	QString m_hijriMonthNumber;
	QString m_hijriYear;
	QString m_hijriDate;
	QString m_gregorianDate;
	QString m_hijriHolidays;

	QString m_FijrTime;
	QString m_DhuhrTime;
	QString m_AsrTime;
	QString m_MaghribTime;
	QString m_IshaTime;

	QString m_sunriseTime;
	QString m_sunsetTime;
	QString m_ImsakTime;
	QString m_midnightTime;
};
Q_DECLARE_METATYPE(CalendarData)
