/*
    HijriCalendar
    Copyright (C) 2020 Abrar

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
    USA
*/

#include <QDebug>
#include <QDateTime>
#include <QMessageBox>
#include <QNetworkReply>
#include <QFile>

#include "calendarinfo.h"
#include "addlocation.h"
#include "location.h"
#include "ui_location.h"

static const QString format = "dd-MM-yyyy";

Location::Location(QWidget *parent) :
	QWidget(parent),
	ui(new Ui::Location)
{
	ui->setupUi(this);
	m_retries = 0;

	nam = new QNetworkAccessManager(this);
	connect(nam, &QNetworkAccessManager::finished, this, &Location::parseNetworkReply);

	ui->shortInfo->setButtons(QList<QToolButton *>() << ui->showInfo << ui->editLoc << ui->deleteLoc);
	on_showInfo_toggled(false);
}

Location::~Location()
{
	delete nam;
	delete ui;
}

QString Location::trayTooltip()
{
	return QString("%1 %2 %3\n%4, %5\n")
	       .arg(m_calendarData.hijriDay())
	       .arg(m_calendarData.hijriMonthName())
	       .arg(m_calendarData.hijriYear())
	       .arg(m_locationData.city())
	       .arg(m_locationData.country());
}

CalendarData Location::calendarData() const
{
	return m_calendarData;
}

void Location::setCalendarData(const CalendarData &calendarData)
{
	m_calendarData = calendarData;
}

LocationData Location::locationData() const
{
	return m_locationData;
}

void Location::setLocationData(const LocationData &locationData)
{
	m_locationData = locationData;
	ui->location->setText(QString("%1, %2").arg(m_locationData.city()).arg(m_locationData.country()));
}

void Location::reloadData()
{
	if (isOld()) {
		qDebug() << "Old data. Requesting new data...";

		m_fp = "/tmp/hijricalendar-" + QDateTime::currentDateTime().toString();
		QString urlStr = "";

//		if (!m_locationData.lat().count() || !m_locationData.lon().count()) {
			urlStr = callURL2(m_locationData.city(), m_locationData.country());
//		} else {
//			urlStr = callURL(m_locationData.lat(), m_locationData.lon());
//		}
qInfo() << urlStr;
		nam->get(QNetworkRequest(QUrl(urlStr)));

		return;
	}

	setData();
}

void Location::setData()
{
	ui->date->setText(QString("%1 %2 %3 %4")
	                  .arg(m_calendarData.hijriWeekday())
	                  .arg(m_calendarData.hijriDay())
	                  .arg(m_calendarData.hijriMonthName())
	                  .arg(m_calendarData.hijriYear()));

	ui->holiday->setText(m_calendarData.hijriHolidays());
	ui->sunrise->setText("Sunrise: " + formatTime(m_calendarData.sunriseTime()));
	ui->sunset->setText("Sunset: " + formatTime(m_calendarData.sunsetTime()));
	ui->imsak->setText("Imsak: " + formatTime(m_calendarData.ImsakTime()));
	ui->midnight->setText("Midnight: " + formatTime(m_calendarData.midnightTime()));
	ui->prayerTimings->setText(QString("%1\n%2\n%3\n%4\n%5")
	                           .arg(formatTime(m_calendarData.FijrTime()))
	                           .arg(formatTime(m_calendarData.DhuhrTime()))
	                           .arg(formatTime(m_calendarData.AsrTime()))
	                           .arg(formatTime(m_calendarData.MaghribTime()))
	                           .arg(formatTime(m_calendarData.IshaTime())));
	emit updated();
}

void Location::parseNetworkReply(QNetworkReply *reply)
{
	QByteArray data = reply->readAll();

	if (data.isEmpty()) {
		// Maybe network not open
		qDebug() << "Might be some network issues retrying..." << ++m_retries;
		if (m_retries >= 3) {
			m_retries = 0;
			setData();
			if (isVisible())
				QMessageBox::warning(this, "Error - Hijri Calendar", "Cannot download calendar data.\nMaybe network error.\nUsing old instead.");
		} else {
			reloadData();
		}
	} else {
		QFile file(m_fp);
		file.open(QIODevice::ReadWrite | QIODevice::Truncate);
		QTextStream in(&file);
		in << data;
		file.close();

		parseRequest();
	}
}

void Location::saveData()
{
	if (m_locationData.lat().count() && m_locationData.lon().count()) {
		QString key = m_locationData.lat() + "," + m_locationData.lon();
		settings->setValue(key + "/locationdata", QVariant::fromValue(m_locationData));
		settings->setValue(key + "/calendardata", QVariant::fromValue(m_calendarData));
		settings->sync();
	}
}

void Location::on_showInfo_toggled(bool checked)
{
	ui->briefInfo->setVisible(checked);

	if (checked) {
		ui->showInfo->setToolTip("Show less info");
		ui->showInfo->setIcon(QIcon::fromTheme("draw-arrow-down"));
	} else {
		ui->showInfo->setToolTip("Show brief info");
		ui->showInfo->setIcon(QIcon::fromTheme("draw-arrow-forward"));
	}
}

void Location::on_editLoc_clicked()
{
	AddLocation *editLoc = new AddLocation(this);
	editLoc->setLabel("Edit Location");
	editLoc->setLocationData(m_locationData);

	if (editLoc->exec() == QDialog::Accepted) {
		setLocationData(editLoc->locationData());
		reloadData();
	}
}

void Location::on_deleteLoc_clicked()
{
	QMessageBox::StandardButton exit = QMessageBox::question(this, "Delete", "Do you want to delete this location?");

	if (exit == QMessageBox::Yes) {
		emit deleted(this);
	}
}

void Location::parseRequest()
{
	CalendarInfo *info = new CalendarInfo(m_fp);
	info->initializeParse();

	m_calendarData = info->getCalendarData();
	m_locationData.setLat(m_calendarData.lat());
	m_locationData.setLon(m_calendarData.lon());

	saveData();
	setData();
}

QString Location::formatTime(QString time)
{
	return QTime::fromString(time, "hh:mm").toString("hh:mm AP");
}

bool Location::isOld()
{
	if (m_calendarData.gregorianDate().count()) {
		QDate temp = QDate::fromString(m_calendarData.gregorianDate(), format);
		QDate curr = QDate::currentDate();
		qDebug() << temp << curr;

		if (temp >= curr) {
			return false;
		}
	}

	return true;
}

QString Location::callURL(QString lat, QString lon)
{
	return QString("https://api.aladhan.com/v1/timings/%1?latitude=%2&longitude=%3&method=4").
		   arg(QDate::currentDate().toString(format)).arg(lat).arg(lon);
}

QString Location::callURL2(QString city, QString country)
{
	return QString("https://api.aladhan.com/v1/timingsByCity/%3?city=%1&country=%2&method=4")
	       .arg(city).arg(country).arg(QDate::currentDate().toString(format));
}
