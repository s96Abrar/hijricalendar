/*
	HijriCalendar
	Copyright (C) 2020 Abrar

	This library is free software; you can redistribute it and/or
	modify it under the terms of the GNU Lesser General Public
	License as published by the Free Software Foundation; either
	version 2.1 of the License, or (at your option) any later version.

	This library is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
	Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public
	License along with this library; if not, write to the Free Software
	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
	USA
*/

#pragma once

#include <QObject>
#include <QDataStream>

class LocationData {
public:
	LocationData();

	friend QDataStream &operator<<(QDataStream &out, const LocationData &data);
	friend QDataStream &operator>>(QDataStream &in, LocationData &data);

	QString country() const;
	void setCountry(const QString &country);

	QString city() const;
	void setCity(const QString &city);

	QString lat() const;
	void setLat(const QString &lat);

	QString lon() const;
	void setLon(const QString &lon);

private:
	QString m_city;
	QString m_country;
	QString m_lat;
	QString m_lon;
};
Q_DECLARE_METATYPE(LocationData)
